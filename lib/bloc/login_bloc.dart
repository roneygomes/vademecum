import 'bloc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:google_sign_in/google_sign_in.dart';

class LoginBloc implements Bloc {
  final GoogleSignIn _googleSignin;
  final PublishSubject<void> _signInController;
  final PublishSubject<GoogleSignInAccount> _accountsController;

  Stream<GoogleSignInAccount> get accountStream => _accountsController.stream;

  LoginBloc(GoogleSignIn googleSignin)
      : _googleSignin = googleSignin,
        _signInController = PublishSubject<void>(),
        _accountsController = PublishSubject<GoogleSignInAccount>() {
    _signInController.listen(_onLogin);
  }

  void dispose() {
    _signInController.close();
  }

  void login() {
    _signInController.sink.add(null);
  }

  void _onLogin(void _) async {
    var account = await _googleSignin.signIn();
    _accountsController.sink.add(account);
  }
}
