import 'package:flutter/material.dart';
import 'view/login/login_page.dart';

void main() => runApp(VadeMecumApp());

class VadeMecumApp extends StatelessWidget {
  @override
  Widget build(BuildContext _) {
    return MaterialApp(
      title: 'Vade Mecum',
      home: LoginPage(),
    );
  }
}
