import 'package:vademecum/view/login/login_view.dart';
import 'package:google_sign_in/google_sign_in.dart';

class LoginPresenter {
  LoginView _view;
  GoogleSignIn _googleSignIn;

  LoginPresenter(this._view, this._googleSignIn);

  Future<void> googleLogin() async {
    final account = await _googleSignIn.signIn();

    if (account != null) {
      _view.onSuccessFulLogin();
    }
  }
}
