import 'package:flutter/material.dart';

class TextImageButton extends StatelessWidget {
  final String uri;
  final String text;
  final Color textColor;

  TextImageButton(this.text, this.uri, this.textColor);

  @override
  Widget build(BuildContext _) {
    return Container(
        width: 160.0,
        child: Center(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image.asset(uri, width: 25.0),
              Text(
                this.text,
                style: TextStyle(
                  fontFamily: 'Roboto',
                  color: textColor,
                ),
              ),
            ],
          ),
        ));
  }
}
