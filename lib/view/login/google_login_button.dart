import 'package:vademecum/view/common/text_image_button.dart';
import 'package:flutter/material.dart';

class GoogleLoginButton extends StatelessWidget {
  final Function onPressed;

  GoogleLoginButton({this.onPressed});

  @override
  Widget build(BuildContext _) {
    return MaterialButton(
      child: TextImageButton(
        'Login com o Google',
        'assets/google.png',
        Color.fromRGBO(68, 68, 76, .8),
      ),
      color: Colors.white,
      onPressed: this.onPressed,
    );
  }
}
