import 'package:flutter/material.dart';
import 'package:vademecum/bloc/bloc_provider.dart';
import 'package:vademecum/bloc/login_bloc.dart';
import 'package:vademecum/view/login/google_login_button.dart';
import 'package:google_sign_in/google_sign_in.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = LoginBloc(GoogleSignIn(scopes: ['email']));

    return Scaffold(
      body: StreamBuilder(
        stream: bloc.accountStream,
        builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
          if (snapshot.hasData) {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("Logged in."),
                ],
              ),
            );
          }

          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                GoogleLoginButton(
                  onPressed: () => bloc.login(),
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
