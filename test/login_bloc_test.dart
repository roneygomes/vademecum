import 'package:test/test.dart';
import 'package:mockito/mockito.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:vademecum/bloc/login_bloc.dart';

class MockedGoogleSignInAccount extends Mock implements GoogleSignInAccount {}

class MockedGoogleSignIn extends Mock implements GoogleSignIn {}

void main() {
  group('LoginBloc tests:', () {
    GoogleSignIn googleSignIn;
    LoginBloc loginBloc;

    setUpAll(() {
      googleSignIn = MockedGoogleSignIn();
      loginBloc = LoginBloc(googleSignIn);

      when(googleSignIn.signIn())
          .thenAnswer((_) async => MockedGoogleSignInAccount());
    });

    tearDownAll(() {
      loginBloc.dispose();
    });

    test('the accounts stream is empty before login', () {
      loginBloc.accountStream.toList().then((list) => expect(list, isEmpty));
    });

    test('the accounts stream has one account after login', () {
      loginBloc.login();

      loginBloc.accountStream.toList().then((list) {
        expect(list.length, 1);
        expect(list.first, equals(MockedGoogleSignInAccount()));
      });
    });
  });
}
