import 'package:test/test.dart';
import 'package:mockito/mockito.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:vademecum/presenter/login/login_presenter.dart';
import 'package:vademecum/view/login/login_view.dart';

class MockedGoogleSignInAccount extends Mock implements GoogleSignInAccount {}

class MockedGoogleSignIn extends Mock implements GoogleSignIn {}

class MockedLoginView extends Mock implements LoginView {}

void main() {
  LoginView loginView;
  GoogleSignIn googleSignIn;
  LoginPresenter loginPresenter;

  setUp(() {
    loginView = MockedLoginView();
    googleSignIn = MockedGoogleSignIn();
    loginPresenter = LoginPresenter(loginView, googleSignIn);
  });

  test('the view is triggered to handle successful logins', () async {
    when(googleSignIn.signIn())
        .thenAnswer((_) async => MockedGoogleSignInAccount());

    await loginPresenter.googleLogin();

    verify(googleSignIn.signIn());
    verify(loginView.onSuccessFulLogin());
  });
}
